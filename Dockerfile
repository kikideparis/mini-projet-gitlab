ARG version="latest"
FROM nginx:${version}

LABEL maintainer="Christian AKELELO"
RUN rm -rf /var/lib/apt/lists/*  # Remove temporary files
RUN apt update && apt install --no-install-recommends -y git && apt clean && rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/diranetafen/static-website-example.git
RUN rm -rf /usr/share/nginx/html/* && mv static-website-example/* /usr/share/nginx/html/

EXPOSE 80

ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off;"]